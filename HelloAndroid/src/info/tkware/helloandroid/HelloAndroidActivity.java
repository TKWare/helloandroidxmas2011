package info.tkware.helloandroid;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.*; // Import ALL the widgets!
import android.media.MediaPlayer;


public class HelloAndroidActivity extends Activity {
	int greetIndex = 0;
	int buttonevent;
	public ProgressBar mProgress;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Button button = (Button) findViewById(R.id.button1);
		mProgress = (ProgressBar) findViewById(R.id.progressBar1);
		mProgress.setVisibility(View.INVISIBLE);
		button.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent buttonevent) {
				if (buttonevent.getAction() == MotionEvent.ACTION_DOWN) {
					mProgress.setVisibility(View.VISIBLE);
					MediaPlayer mp = MediaPlayer.create(getApplication(), R.raw.xmasmemories);
					mp.start();
					greetingShenanigans();
					return true;
				}
				return false;
			}
		});
	}
	void greetingShenanigans() {
		String[] greetArray;
		greetArray = new String[9];
		greetArray[0] = "TK Says: Hello world!";
		greetArray[1] = "This is a greeting app. And now for the greetings:";
		greetArray[2] = "Shincks: Love ya to death, merry christmas, hope the new job works out!";
		greetArray[3] = "Seanie: Good job Microsoft, eh? I won't get so behind on my payments in 2012. It's on my new years resolutions.";
		greetArray[4] = "Ayano: Love the tower, thanks for introducing me to Psych Today!";
		greetArray[5] = "Erik: Mind the sheep email this app just sent to everyone in your address book :D";
		greetArray[6] = "(just kidding!)";
		greetArray[7] = "Jeremy: Thank god you didn't end up holding the pager again over the holiday. That's no fun at all :3";
		greetArray[8] = "And that's about it. Thanks for installing me!";
		for (greetIndex=0; greetIndex<9; greetIndex++)
		{
			Toast.makeText(HelloAndroidActivity.this, greetArray[greetIndex], Toast.LENGTH_LONG).show();
		}
	}		
}


